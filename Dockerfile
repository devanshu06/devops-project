FROM ubuntu
RUN apt-get update
RUN apt-get install nodejs -y
RUN apt-get install git -y
RUN apt-get install npm -y
RUN git clone https://gitlab.com/devanshu06/assignment_app.git
WORKDIR /assignment_app/app
RUN npm install
CMD node src/index.js
